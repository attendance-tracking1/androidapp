package com.valentun.testbeacons.extensions

import android.app.Activity
import android.content.Context
import android.os.Build
import android.text.Editable
import android.text.Html
import android.text.Spanned
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.webkit.WebView
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.fragment.app.Fragment
import com.valentun.testbeacons.App
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


fun Activity.hideKeyboard() {
    val view = currentFocus
    view?.hideKeyboard()
}

@Suppress("DEPRECATION")
fun String.formatted() : Spanned {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(this, Html.FROM_HTML_MODE_COMPACT)
    } else {
        Html.fromHtml(this)
    }
}

fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

fun View.showKeyboard() {
    requestFocus()

    val keyboard = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

    keyboard.showSoftInput(this, 0)
}

fun EditText.onTextChanged(action: (String) -> Unit) {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable) {
            //pass
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            // pass
        }

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            action.invoke(s.toString())
        }
    })
}

private val dateFormat : DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.UK)

fun getCurrentTimeStamp(): String = dateFormat.format(Date())

val TextView.content
    get() = this.text.toString()

fun String.toReadableDate(): String {
    val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())

    val date = dateFormat.parse(this) ?: throw IllegalArgumentException("$this is in wrong date format")

    val calendar = Calendar.getInstance()
    calendar.time = date

    return calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault())!!
}

@Suppress("unused")
fun Fragment.getColor(@ColorRes colorRes: Int): Int {
    return App.getColor(colorRes)
}

fun WebView.loadHtml(html: String) {
    loadDataWithBaseURL("", html, "text/html", "UTF-8", "")
}