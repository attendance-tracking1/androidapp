package com.valentun.testbeacons.di

import com.valentun.testbeacons.data.Repository
import com.valentun.testbeacons.data.actual.ActualRepository
import com.valentun.testbeacons.data.local.LocalDataManager
import org.koin.dsl.module


@Suppress("RemoveExplicitTypeArguments")
val dataModule = module {
    single<Repository> { ActualRepository(api = get(), localManager = get()) }

    single<LocalDataManager> { LocalDataManager(get(), get()) }
}
