package com.valentun.testbeacons.ui.teacherMode

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.DividerItemDecoration.VERTICAL
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.recyclical.datasource.emptyDataSource
import com.afollestad.recyclical.setup
import com.afollestad.recyclical.withItem
import com.valentun.testbeacons.R
import com.valentun.testbeacons.data.Repository
import com.valentun.testbeacons.data.network.errorMessage
import com.valentun.testbeacons.data.pojo.Attendee
import com.valentun.testbeacons.ui.BaseActivity
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.activity_attendees.*
import kotlinx.android.synthetic.main.item_attendee.view.*
import org.koin.android.ext.android.inject

private const val STATE_PROGRESS = 0
private const val STATE_CONTENT = 1
private const val STATE_ERROR = 2

const val EXTRA_ROOM = "EXTRA_ROOM"

class AttendeeHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer

class AttendeesActivity : BaseActivity() {

    private val repository : Repository by inject()

    private val dataSource = emptyDataSource()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_attendees)

        setupRecyclerView()

        loadData()

        actionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setupRecyclerView() {
        attendeesList.setHasFixedSize(true)

        val emptyView = layoutInflater.inflate(R.layout.list_empty_placeholder, attendeesList, false)

        attendeesList.setup {
            withDataSource(dataSource)

            withItem<Attendee, AttendeeHolder>(R.layout.item_attendee) {
                onBind(::AttendeeHolder) { _, item ->
                    containerView.attendeeName.text = item.name
                }
            }

            withEmptyView(emptyView)
        }

        attendeesList.addItemDecoration(DividerItemDecoration(this, VERTICAL))
    }

    private fun loadData() {
        launch ({
            val statistics = repository.getAttendees(getRoomNumber())

            showAttendees(statistics.students)
        }, { error ->
            attendeesFlipper.displayedChild = STATE_ERROR
            errorContent.text = error.errorMessage
        })
    }

    private fun getRoomNumber() = intent.extras!![EXTRA_ROOM] as String

    private fun showAttendees(attendees: List<Attendee>) {
        attendeesFlipper.displayedChild = STATE_CONTENT
        dataSource.set(attendees)
    }
}
