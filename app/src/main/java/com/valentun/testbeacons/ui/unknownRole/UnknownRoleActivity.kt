package com.valentun.testbeacons.ui.unknownRole

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.valentun.testbeacons.R

class UnknownRoleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_unknown_role)
    }
}
