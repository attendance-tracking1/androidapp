package com.valentun.testbeacons.ui.login

import android.os.Bundle
import android.view.View
import com.valentun.testbeacons.R
import com.valentun.testbeacons.data.Repository
import com.valentun.testbeacons.data.network.errorMessage
import com.valentun.testbeacons.extensions.content
import com.valentun.testbeacons.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_verification.*
import org.koin.android.ext.android.inject

class VerificationActivity : BaseActivity() {

    private val repository : Repository by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verification)

        verificationConfirm.setOnClickListener { confirmClicked() }
    }

    private fun confirmClicked() {
        val code = verificationInput.content

        verify(code)
    }

    private fun verify(code: String) = launch ({
        verificationProgress.visibility = View.VISIBLE

        repository.verifyAccount(code)

        val currentSession = repository.getUserSession()!!

        val newSession = currentSession.copy(isVerified = true)

        repository.updateUserSession(newSession)

        newRootScreen(getScreenForSession(newSession))
    }, {
        showMessage(it.errorMessage)
        verificationProgress.visibility = View.GONE
    })
}
