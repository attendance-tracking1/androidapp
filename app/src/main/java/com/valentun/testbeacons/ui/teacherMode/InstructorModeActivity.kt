package com.valentun.testbeacons.ui.teacherMode

import android.bluetooth.le.AdvertiseSettings.ADVERTISE_MODE_LOW_LATENCY
import android.os.Bundle
import android.view.View
import com.valentun.testbeacons.R
import com.valentun.testbeacons.extensions.content
import com.valentun.testbeacons.extensions.onTextChanged
import com.valentun.testbeacons.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_teacher_mode.*
import org.altbeacon.beacon.Beacon
import org.altbeacon.beacon.BeaconParser
import org.altbeacon.beacon.BeaconTransmitter
import org.jetbrains.anko.startActivity


class InstructorModeActivity : BaseActivity() {

    var transmitter: BeaconTransmitter? = null

    private var isSending = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_teacher_mode)

        init()

        instructorOpenAttendees.setOnClickListener {
            val room = instructorAuditorium.content

            startActivity<AttendeesActivity>(EXTRA_ROOM to room)
        }

        instructorAuditorium.onTextChanged {
            val isValid = it.isNotEmpty()

            instructorTrack.isEnabled = isValid
            instructorOpenAttendees.isEnabled = isValid

            val attendeesText = if (isValid) {
                getString(R.string.action_view_attendees, it)
            } else {
                getString(R.string.action_view_attendees_placeholder)
            }

            instructorOpenAttendees.text = attendeesText
        }

        instructorTrack.setOnClickListener {
            if (isSending) {
                stopAdvertising()
            } else {
                tryAdvertising()
            }

            updateViewState()
        }
    }

    private fun updateViewState() {
        val textRes = if (isSending) R.string.stop else R.string.start

        instructorTrack.setText(textRes)

        instructorAuditorium.isEnabled = !isSending

        val sendingStatusVisibility = if (isSending) View.VISIBLE else View.GONE

        instructorTracking.visibility = sendingStatusVisibility
    }

    private fun tryAdvertising() {
        if (BeaconTransmitter.checkTransmissionSupported(this) != BeaconTransmitter.SUPPORTED) {
            showMessage(R.string.sending_not_supported)
        } else {
            transmitter?.startAdvertising()

            isSending = true
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        stopAdvertising()
    }

    private fun stopAdvertising() {
        transmitter?.stopAdvertising()

        isSending = false
    }

    private fun init() {
        val id = instructorAuditorium.content

        val beacon = Beacon.Builder()
            .setId1(id)
            .setId2("1")
            .setId3("2")
            .setManufacturer(0x0118)
            .setTxPower(-59)
            .setDataFields(listOf(0L))
            .build()

        val beaconParser = BeaconParser()
            .setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24")

        transmitter =
            BeaconTransmitter(applicationContext, beaconParser)

        transmitter!!.stopAdvertising()
        transmitter!!.advertiseMode = ADVERTISE_MODE_LOW_LATENCY

        transmitter!!.setBeacon(beacon)
    }
}
