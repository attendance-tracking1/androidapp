package com.valentun.testbeacons.ui.login

import android.os.Bundle
import android.view.View
import com.valentun.testbeacons.R
import com.valentun.testbeacons.data.Repository
import com.valentun.testbeacons.data.network.errorMessage
import com.valentun.testbeacons.data.pojo.Credentials
import com.valentun.testbeacons.extensions.content
import com.valentun.testbeacons.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.ext.android.inject

class LoginActivity : BaseActivity() {

    private val repository : Repository by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loginConfirm.setOnClickListener { confirmClicked() }
    }

    private fun confirmClicked() {
        val email = loginEmail.content
        val password = loginPassword.content

        val credentials = Credentials(email, password)

        performLoginAttempt(credentials)
    }

    private fun performLoginAttempt(credentials: Credentials) = launch ({
        loginProgress.visibility = View.VISIBLE

        val userSession = repository.login(credentials)

        repository.updateUserSession(userSession)

        val nextScreen = getScreenForSession(userSession)

        newRootScreen(nextScreen)
    }, { error ->
        loginProgress.visibility = View.GONE

        showMessage(error.errorMessage)
    })
}
