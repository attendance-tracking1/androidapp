package com.valentun.testbeacons.ui.studentMode

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.valentun.testbeacons.R
import com.valentun.testbeacons.data.Repository
import com.valentun.testbeacons.data.network.errorMessage
import com.valentun.testbeacons.ui.BaseActivity
import kotlinx.android.synthetic.main.activity_student_mode.*
import org.altbeacon.beacon.*
import org.koin.android.ext.android.inject

private const val KEY_DATA_SENT = "KEY_DATA_SENT"

class StudentModeActivity : BaseActivity(), BeaconConsumer, RangeNotifier {

    private lateinit var beaconManager: BeaconManager

    private var isDataSend = false

    private var nearestBeacon : Beacon? = null

    private val repository: Repository by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_student_mode)

        beaconManager = BeaconManager.getInstanceForApplication(this)

        beaconManager.beaconParsers.add(BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24"))

        beaconManager.bind(this)

        savedInstanceState?.let {
            isDataSend = it[KEY_DATA_SENT] as Boolean
        }

        receiveSubmit.setOnClickListener { checkIn() }
    }

    private fun checkIn() {
        launch ({
            repository.checkIn(nearestBeacon!!.id1.toString())
        }, { error ->
            showMessage(error.errorMessage)
        })
    }

    override fun onBeaconServiceConnect() {
        beaconManager.removeAllMonitorNotifiers()

        beaconManager.addRangeNotifier(this)

        beaconManager.startRangingBeaconsInRegion(Region("myRangingUniqueId", null, null, null))

    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean(KEY_DATA_SENT, isDataSend)

        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        isDataSend = savedInstanceState[KEY_DATA_SENT] as Boolean

        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun didRangeBeaconsInRegion(beacons: MutableCollection<Beacon>, region: Region) {
        val nearest = beacons.minBy { it.distance }

        if (nearest != null && !isDataSend)  {
            nearestBeacon = nearest
            receiveSubmit.isEnabled = true
            receiveResult.text = getString(R.string.scan_result_format, nearest.id1.toString())
        }
    }
}
