package com.valentun.testbeacons.ui

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.valentun.testbeacons.data.network.errorMessage
import com.valentun.testbeacons.data.pojo.ROLE_INSTRUCTOR
import com.valentun.testbeacons.data.pojo.ROLE_STUDENT
import com.valentun.testbeacons.data.pojo.UserSession
import com.valentun.testbeacons.ui.login.VerificationActivity
import com.valentun.testbeacons.ui.studentMode.StudentModeActivity
import com.valentun.testbeacons.ui.teacherMode.InstructorModeActivity
import com.valentun.testbeacons.ui.unknownRole.UnknownRoleActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.*

abstract class BaseActivity :AppCompatActivity() {
    protected fun showMessage(message: String) {
        contentView?.let {
            Snackbar.make(it, message, Snackbar.LENGTH_SHORT)
                .show()
        }
    }

    protected fun showMessage(messageRes: Int) {
        showMessage(getString(messageRes))
    }

    private val defaultCatch: suspend CoroutineScope.(Throwable) -> Unit = {
        showMessage(it.errorMessage)
    }

    protected fun launch(tryBlock: suspend CoroutineScope.() -> Unit,
                         catchBlock: suspend CoroutineScope.(Throwable) -> Unit) {
        GlobalScope.launch(Dispatchers.Main) {
            try {
                tryBlock()
            } catch (e: Throwable) {
                catchBlock(e)
            }
        }
    }

    protected fun launch(tryBlock: suspend CoroutineScope.() -> Unit) {
        launch(tryBlock, defaultCatch)
    }


    protected fun getScreenForSession(userSession: UserSession) : Class<out AppCompatActivity> {
        if (!userSession.isVerified && userSession.role == ROLE_STUDENT) {
            return VerificationActivity::class.java
        }

        return when (userSession.role) {
            ROLE_STUDENT -> StudentModeActivity::class
            ROLE_INSTRUCTOR -> InstructorModeActivity::class
            else -> UnknownRoleActivity::class
        }.java
    }

    protected fun newRootScreen(nextScreen: Class<out AppCompatActivity>) {
        val intent = Intent(this, nextScreen)
            .clearTask()
            .newTask()

        startActivity(intent)
    }

    protected inline fun <reified T: Activity> openScreen() {
        startActivity<T>()
    }
}