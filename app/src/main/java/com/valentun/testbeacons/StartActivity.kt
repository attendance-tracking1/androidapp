package com.valentun.testbeacons

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.valentun.testbeacons.data.Repository
import com.valentun.testbeacons.ui.BaseActivity
import com.valentun.testbeacons.ui.login.LoginActivity
import org.koin.android.ext.android.inject

private const val LOCATION_CODE = 1

class StartActivity : BaseActivity() {

    private val repository: Repository by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)

        checkLocationPermission()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when(requestCode) {
            LOCATION_CODE ->  {
                if (grantResults[0] == PERMISSION_GRANTED) {
                    continueToNextScreen()
                } else {
                    askPermissions()
                }
            }
        }
    }

    private fun checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                ACCESS_FINE_LOCATION
            ) != PERMISSION_GRANTED
        ) {
            askPermissions()
        } else {
            continueToNextScreen()
        }
    }

    private fun continueToNextScreen() {
        val nextScreen = if (repository.isUserSessionStarted()) {
            getScreenForSession(repository.getUserSession()!!)
        } else {
            LoginActivity::class.java
        }

        newRootScreen(nextScreen)
    }

    private fun askPermissions() {
        ActivityCompat.requestPermissions(this, arrayOf(ACCESS_FINE_LOCATION), LOCATION_CODE)
    }
}
