package com.valentun.testbeacons

import android.app.Application
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.valentun.testbeacons.di.dataModule
import com.valentun.testbeacons.di.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {
    companion object {
        lateinit var INSTANCE: App

        fun getString(@StringRes stringRes: Int): String {
            return INSTANCE.getString(stringRes)
        }

        fun getColor(@ColorRes colorRes: Int): Int {
            return ContextCompat.getColor(INSTANCE, colorRes)
        }
    }

    override fun onCreate() {
        super.onCreate()

        INSTANCE = this

        startKoin {
            androidContext(this@App)

            modules(listOf(dataModule, networkModule))
        }
    }
}