package com.valentun.testbeacons.data.actual

import com.valentun.testbeacons.data.Repository
import com.valentun.testbeacons.data.local.LocalDataManager
import com.valentun.testbeacons.data.network.ApiService
import com.valentun.testbeacons.data.pojo.*
import com.valentun.testbeacons.extensions.getCurrentTimeStamp
import kotlinx.coroutines.delay


class ActualRepository(val api: ApiService,
                       val localManager: LocalDataManager) : Repository {

    override fun finishUserSession() {
        localManager.clearUserSession()
    }

    override suspend fun getAttendees(room: String): AttendanceStatistics {
        val datetime = getCurrentTimeStamp()

        return api.getAttendeesAsync(room, datetime).await()
    }

    override suspend fun verifyAccount(code: String) {
        val session = localManager.getUserSession()!!
        val verification = Verification(session.userId, code, session.role)

        api.verifySessionAsync(verification).await()
    }

    override suspend fun checkIn(beaconId: String) {
        val datetime = getCurrentTimeStamp()

        api.checkIn(AttendanceRecord(beaconId, datetime))
    }

    override fun isUserSessionStarted() = localManager.isAuthenticated()

    override fun getUserSession() = localManager.getUserSession()

    override suspend fun login(credentials: Credentials): UserSession {
        return api.loginAsync(credentials).await()
    }

    override fun updateUserSession(userSession: UserSession) {
        localManager.saveUserSession(userSession)
    }
}