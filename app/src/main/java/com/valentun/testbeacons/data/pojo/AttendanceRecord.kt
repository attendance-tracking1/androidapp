package com.valentun.testbeacons.data.pojo

import com.fasterxml.jackson.annotation.JsonProperty

class AttendanceRecord(@JsonProperty("beaconid") val beaconId: String,
                       @JsonProperty("datetime") val timeStamp: String)