package com.valentun.testbeacons.data

import com.valentun.testbeacons.data.pojo.*

interface Repository {
    suspend fun login(credentials: Credentials) : UserSession

    fun updateUserSession(userSession: UserSession)
    fun getUserSession() : UserSession?
    fun isUserSessionStarted() : Boolean
    fun finishUserSession()

    suspend fun getAttendees(room: String): AttendanceStatistics

    suspend fun verifyAccount(code: String)
    suspend fun checkIn(beaconId: String)
}