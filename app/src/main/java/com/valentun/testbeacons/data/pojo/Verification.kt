package com.valentun.testbeacons.data.pojo

import com.fasterxml.jackson.annotation.JsonProperty

class Verification(@JsonProperty("_id") val id: String, val code: String, val role: String)