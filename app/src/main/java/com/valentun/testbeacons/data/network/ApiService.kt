package com.valentun.testbeacons.data.network

import com.valentun.testbeacons.data.pojo.*
import kotlinx.coroutines.Deferred
import okhttp3.Response
import okhttp3.ResponseBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query


interface ApiService {

    @POST("login")
    fun loginAsync(@Body auth: Credentials): Deferred<UserSession>

    @POST("confirm_verification")
    fun verifySessionAsync(@Body body: Verification): Deferred<ResponseBody>

    @GET("/api/attendancetracking")
    fun getAttendeesAsync(@Query ("roomNumber") beaconId: String,
                          @Query("datetime") datetime: String): Deferred<AttendanceStatistics>

    @POST("/api/attendancetracking")
    fun checkIn(@Body body: AttendanceRecord)
}