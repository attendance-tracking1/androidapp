package com.valentun.testbeacons.data.pojo

import com.fasterxml.jackson.annotation.JsonProperty

const val ROLE_STUDENT = "student"
const val ROLE_INSTRUCTOR = "teacher"

data class UserSession(@JsonProperty("_id") val userId: String,
                       val token: String,
                       val role: String,
                       val isVerified: Boolean = false)