package com.valentun.testbeacons.data.pojo

class AttendanceStatistics(val students: List<Attendee>)

class Attendee(val name: String)