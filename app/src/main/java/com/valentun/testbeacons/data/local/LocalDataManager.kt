package com.valentun.testbeacons.data.local

import android.content.Context
import androidx.preference.PreferenceManager
import com.fasterxml.jackson.databind.ObjectMapper
import com.valentun.testbeacons.data.pojo.UserSession

private const val KEY_AUTH_TOKEN = "KEY_AUTH_TOKEN"


class LocalDataManager(context: Context,
                       private val mapper: ObjectMapper) {
    private val preferences = PreferenceManager.getDefaultSharedPreferences(context)

    fun getUserSession(): UserSession? {
        val raw = preferences.getString(KEY_AUTH_TOKEN, null)
            ?: return null

        return mapper.readValue(raw, UserSession::class.java)
    }

    fun isAuthenticated() = preferences.contains(KEY_AUTH_TOKEN)

    fun clearUserSession() {
        preferences.edit()
            .remove(KEY_AUTH_TOKEN)
            .apply()
    }

    fun saveUserSession(userSession: UserSession) {
        val raw = mapper.writeValueAsString(userSession)

        preferences.edit()
            .putString(KEY_AUTH_TOKEN, raw)
            .apply()
    }
}